/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : vue.config.js
 * @Created_at  : 06/02/2021
 * @Update_at   : 06/02/2021
 * ----------------------------------------------------------------
 */

module.exports = {
    css: {
        extract: false
    }
};
