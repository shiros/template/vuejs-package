/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : main-lib.js
 * @Created_at  : 06/02/2021
 * @Update_at   : 06/02/2021
 * ----------------------------------------------------------------
 */

// Require common css
import "./assets/css/Variables.css';

// Require needed elements - Components
import MyComponent from "./components/MyComponent";

// --------------------------------
// Export elements

export {
    // Components
    MyComponent
};

// --------------------------------
// Export as plugin

export default {
    install (Vue, options = {}) {
        // TODO : Do somethings.
    }
};
