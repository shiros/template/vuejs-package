/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : routes.js
 * @Created_at  : 06/02/2021
 * @Update_at   : 06/02/2021
 * ----------------------------------------------------------------
 */

// Components
import TestMyComponent from "@/tests/TestMyComponent";

export default [
    // --------------------------------
    // Page : Test

    {
        path: '/test/my-component',
        name: 'test_myComponent',
        component: TestMyComponent
    }
];