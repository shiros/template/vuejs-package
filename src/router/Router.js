/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : Router.js
 * @Created_at  : 06/02/2021
 * @Update_at   : 06/02/2021
 * ----------------------------------------------------------------
 */

// Require needed libraries
import Vue from 'vue';
import VueRouter from 'vue-router';

// Require needed components
import routes from './routes/routes';

export default class Router {
    // --------------------------------
    // Constants

    /**
     * Get Vue Router.
     *
     * @returns {VueRouter}
     */
    static get router() {
        return new VueRouter({
            mode: 'history',
            routes: routes
        });
    }

    // --------------------------------
    // Register

    /**
     * Register the router module in VueJs.
     */
    static register() {
        Vue.use(VueRouter);
    }
}
