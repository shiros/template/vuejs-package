/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : main.js
 * @Created_at  : 06/02/2021
 * @Update_at   : 21/02/2021
 * ----------------------------------------------------------------
 */

import Vue from 'vue';

import App from './App';

import Router from './router/Router';

// --------------------------------
// Configs

Vue.config.productionTip = false;
Vue.config.performance = true;

// --------------------------------
// Register Modules

// Router
Router.register();

// --------------------------------
// Constants

// App
const rootElement = '#app';

// Router
const router = Router.router;

// --------------------------------
// Entry Point

new Vue({
    router: router,
    render: h => h(App)
}).$mount(rootElement);
