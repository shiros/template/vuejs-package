# Template - VueJs Package

This is a template for VueJs package.

## Project

Some utils commands.

```shell script
$ yarn install
$ yarn serve
$ yarn build
$ yarn lint
$ yarn test
```

## Authors

- Alexandre Caillot (Shiroe_sama) 